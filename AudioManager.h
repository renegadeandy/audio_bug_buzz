
#ifndef Audio_Manager_h
#define Audio_Manager_h

#include "Arduino.h"
#include "SPIFFS.h"
#include "AudioFileSourceSPIFFS.h"
#include "AudioFileSourceID3.h"
#include "AudioGeneratorMP3.h"
#include "AudioOutputI2S.h"

#include "enums.h"
#include "sfx.h"

 extern char* snes_sfx[];
 
class AudioManager
{


  
  public:
    AudioManager(); //Constructor
    void begin();//Starts Audio System
    void playStartupMachine(enum machines machine); //kicks off an audio track for system startup according to system specified
    boolean handleAudio();// handles Playing of audio if required to be called each tick
    

  private:
    void AMPrintf(String);
  
};
  void MDCallback(void *cbData, const char *type, bool isUnicode, const char *string);
#endif
