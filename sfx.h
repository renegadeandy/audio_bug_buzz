#ifndef SFX_H
#define SFX_H
static char* snes_sfx[10] = {"/music/sfx/smb3_pause.mp3",
                            "/music/sfx/smb3_player_down.mp3",
                            "/music/sfx/smb3_whistle.mp3",
                            "/music/sfx/smsunshine_mario_here_we_go.mp3",
                            "/music/sfx/smsunshine_mario_jump_wahaa.mp3",
                            "/music/sfx/smw_1-up.mp3",
                            "/music/sfx/smw_coin.mp3",
                            "/music/sfx/smw_jump.mp3",
                            "/music/sfx/smw_power-up.mp3",
                            "/music/sfx/smw_yoshi_stomp.mp3"};
#endif
