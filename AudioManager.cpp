#include "AudioManager.h"

AudioGeneratorMP3 *mp3;
AudioFileSourceSPIFFS *file;
AudioOutputI2S *out;
AudioFileSourceID3 *id3;

boolean readyToPlay = false;
AudioManager::AudioManager() {
  this->AMPrintf("Initialsed the AudioManager object");
}

void AudioManager::begin() {
   this->AMPrintf("Starting AudioManager");
  SPIFFS.begin();
  this->AMPrintf("AudioManager startup complete");
}

void AudioManager::playStartupMachine(enum machines machine){
 this->AMPrintf("Playing audio for machine: "+String(machine));
int choice = 0;
  if(machine == one){
      choice = random(0, 10);
      file = new AudioFileSourceSPIFFS(snes_sfx[choice]);
      this->AMPrintf("Audio track choice is: "+String(snes_sfx[choice]));
  }

 readyToPlay = true;
  id3 = new AudioFileSourceID3(file);
  id3->RegisterMetadataCB(MDCallback, (void*)"ID3TAG");
  out = new AudioOutputI2S();
  out->SetPinout(5,16,17);
  mp3 = new AudioGeneratorMP3();
  mp3->begin(id3, out);
  
}

boolean AudioManager::handleAudio(){
  if(readyToPlay){
    if (mp3->isRunning()) {
      if (!mp3->loop()){
        mp3->stop();
        return true;
      }
    }else {
   // readyToPlay = false;
    }
  }
  return false;
}

// Called when a metadata event occurs (i.e. an ID3 tag, an ICY block, etc.
void MDCallback(void *cbData, const char *type, bool isUnicode, const char *string)
{
  (void)cbData;
  Serial.printf("ID3 callback for: %s = '", type);

  if (isUnicode) {
    string += 2;
  }
  
  while (*string) {
    char a = *(string++);
    if (isUnicode) {
      string++;
    }
    Serial.printf("%c", a);
     Serial.printf("\n");
      Serial.flush();
  }
}


void AudioManager::AMPrintf(String logMsg) {
  Serial.println("AM::" + logMsg);
}


