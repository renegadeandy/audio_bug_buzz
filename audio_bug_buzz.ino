#include "AudioManager.h"

int relayPin1=22;
boolean done=false;
AudioManager am;

void setup() {
  Serial.begin(115200);
  //Start AudioManager
  Serial.println("Setting up AudioManager...");
  am.begin();
  Serial.println("AudioManager setup complete");
    
}

void loop() {
  // put your main code here, to run repeatedly:
 //Turn on relay 1
   if(!done){
     digitalWrite(relayPin1, LOW);
     am.playStartupMachine(one);
    done = true;
   }

  //Play audio if required
  am.handleAudio();
  //Check DiscoverMe status
 
}
